<?php

use App\Liga;
use Illuminate\Database\Seeder;

class LigasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Liga::create([
            'nombre' => 'LIGA 1',
            'fecha_inicio' => '2021-01-01',
            'fecha_termino' => '2021-03-01',
            'cantidad_juegos' => 5,
            'id_tipo_liga' => 1
        ]);

        Liga::create([
            'nombre' => 'LIGA 2',
            'fecha_inicio' => '2021-05-01',
            'fecha_termino' => '2021-06-30',
            'cantidad_juegos' => 4,
            'id_tipo_liga' => 1
        ]);
        Liga::create([
            'nombre' => 'LIGA 3',
            'fecha_inicio' => '2021-07-01',
            'fecha_termino' => '2021-08-15',
            'cantidad_juegos' => 4,
            'id_tipo_liga' => 1
        ]);
        Liga::create([
            'nombre' => 'LIGA 4',
            'fecha_inicio' => '2021-09-15',
            'fecha_termino' => '2021-10-15',
            'cantidad_juegos' => 4,
            'id_tipo_liga' => 1
        ]);
        Liga::create([
            'nombre' => 'LIGA 5',
            'fecha_inicio' => '2021-11-15',
            'fecha_termino' => '2021-12-15',
            'cantidad_juegos' => 2,
            'id_tipo_liga' => 1
        ]);
    }
}
