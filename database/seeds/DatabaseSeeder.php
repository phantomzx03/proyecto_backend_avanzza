<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(TiposLigasTableSeeder::class);
        $this->call(LigasTableSeeder::class);
        $this->call(EquiposTableSeeder::class);
        $this->call(JugadoresTableSeeder::class);
        $this->call(ApisCodigosTableSeeder::class);
    }
}
