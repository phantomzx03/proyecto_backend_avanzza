<?php

use App\TipoLiga;
use Illuminate\Database\Seeder;

class TiposLigasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        TipoLiga::create([
            'nombre' => 'NACIONAL',
        ]);
    }
}
