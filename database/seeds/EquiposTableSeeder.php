<?php

use App\Equipo;
use Illuminate\Database\Seeder;

class EquiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i = 1; $i <= 6; $i++) {
            Equipo::create([
                'nombre' => 'Equipo ' . $i
            ]);
        }
    }
}
