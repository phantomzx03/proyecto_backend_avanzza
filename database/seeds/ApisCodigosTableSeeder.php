<?php

use App\ApiCodigo;
use Illuminate\Database\Seeder;

class ApisCodigosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ApiCodigo::create([
            'token'=>'$2y$10$LVJWOYUYDjzqbvN8L.E2auGQEHxEiRa/mYSiHGWakFE/9N1USansa',
            'tipo'=>'L'
        ]);
        ApiCodigo::create([
            'token'=>'$2y$10$MKJxX2GV/BHCA97Q677rYOUg3q.OVoBkQogeNERs3JrE59LlNfrH6',
            'tipo'=>'P'
        ]);
    }
}
