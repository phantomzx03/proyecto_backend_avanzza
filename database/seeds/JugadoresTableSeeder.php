<?php

use App\Jugador;
use Illuminate\Database\Seeder;

class JugadoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '1',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => 1
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '2',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => 2
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '3',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => 4
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '4',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => 3
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '5',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => NULL
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '6',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => 4
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '7',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => 4
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '8',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => 4
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '9',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => 5
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '10',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => NULL
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '11',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => 2
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '12',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => NULL
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '13',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => NULL
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '14',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => NULL
        ]);
        Jugador::create([
            'nombres' => 'Jugador',
            'apellidos' => '15',
            'fecha_nacimiento' => '2021-08-26',
            'id_equipo' => NULL
        ]);
    }
}
