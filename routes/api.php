<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['middleware' => ['apiThrottle:3,1']], function () {
    Route::group(['prefix' => 'ligas'], function () {
        Route::get('/', 'Api\LigaController@obtenerTodos');
        Route::get('/{id}/equipos', 'Api\LigaController@obtenerEquipos');
        Route::post('/guardar-equipos', 'Api\LigaController@guardarEquipos');
    });
    Route::group(['prefix' => 'equipos'], function () {
        Route::get('/', 'Api\EquipoController@obtenerTodos');
        Route::get('/{id}/ligas', 'Api\EquipoController@obtenerLigas');
        Route::get('/{id}/jugadores', 'Api\EquipoController@obtenerJugadores');
    });
    Route::group(['prefix' => 'jugadores'], function () {
        Route::post('/guardar', 'Api\JugadorController@guardarJugador');
        Route::put('/{id}/asignar-equipo', 'Api\JugadorController@asignarEquipo');
    });
});
