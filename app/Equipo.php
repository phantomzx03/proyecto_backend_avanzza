<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    //
    protected $table = 'equipos';

    protected $primaryKey = 'id';

    protected  $fillable = ['nombre'];

    public $timestamps = false;

    /**
     * Las Ligas que pertenecen al Equipo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ligas()
    {
        return $this->belongsToMany('App\Liga', 'equipos_ligas', 'id_equipo', 'id_liga');
    }

    /**
     * Traer todos Jugadores de un Equipo
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jugadores()
    {
        return $this->hasMany('App\Jugador', 'id_equipo', 'id');
    }
}
