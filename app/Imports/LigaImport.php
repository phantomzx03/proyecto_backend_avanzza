<?php

namespace App\Imports;

use App\Liga;
use DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class LigaImport implements ToCollection, WithHeadingRow, WithBatchInserts
{
    public function collection(Collection $rows)
    {
        $filas_agrupadas = $rows->groupBy('id_liga');
        DB::beginTransaction();
        try {
            foreach ($filas_agrupadas as $key => $fila)
            {
                $data = [];
                foreach($fila as $fila_data){
                    $data[] = $fila_data['id_equipo'];
                }
                $liga = Liga::findOrFail($key);
                $liga->equipos()->attach($data);
            }
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function batchSize(): int
    {
        return 1000;
    }
}
