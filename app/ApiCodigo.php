<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiCodigo extends Model
{
    //
    protected $table = 'api_codigos';

    protected $primaryKey = 'id';

    protected  $fillable = ['token','tipo'];

    public $timestamps = false;
}
