<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liga extends Model
{
    //
    protected $table = 'ligas';

    protected $primaryKey = 'id';

    protected  $fillable = ['nombre','fecha_inicio','fecha_termino','cantidad_juegos'];

    public $timestamps = false;

    /**
     * Traer TipoLiga asociado a la Liga
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tipo()
    {
        return $this->hasOne('App\TipoLiga', 'id', 'id_tipo_liga');
    }

    /**
     * Los equipos que pertenecen a la Liga
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function equipos()
    {
        return $this->belongsToMany('App\Equipo', 'equipos_ligas', 'id_liga', 'id_equipo');
    }
}
