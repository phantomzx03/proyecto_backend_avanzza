<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Equipo;

class EquipoController extends Controller
{
    //
    public function obtenerTodos()
    {
        return response()->json(['status' => 'success', 'equipos' => Equipo::all()]);
    }

    public function obtenerLigas($id)
    {
        $equipo = Equipo::where('id', $id)->with('ligas')->firstOrFail();
        return response()->json(['status' => 'success', 'equipo' => $equipo]);
    }

    public function obtenerJugadores($id)
    {
        $equipo = Equipo::where('id', $id)->with('jugadores')->firstOrFail();
        return response()->json(['status' => 'success', 'equipo' => $equipo]);
    }

}
