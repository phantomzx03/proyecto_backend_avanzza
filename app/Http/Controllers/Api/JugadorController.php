<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AsignarEquipoRequest;
use App\Http\Requests\Api\GuardarJugadorRequest;
use App\Jugador;

class JugadorController extends Controller
{
    //
    public function guardarJugador(GuardarJugadorRequest $request)
    {
        if ($request->isJson()) {
            $validated = $request->validated();
            $jugador = new Jugador;
            $jugador->nombres = $request->nombres;
            $jugador->apellidos = $request->apellidos;
            $jugador->fecha_nacimiento = $request->fecha_nacimiento;
            $jugador->saveOrFail();
            return response()->json(['status' => 'success', 'jugador' => $jugador], 201);
        }
        return response()->json(['message' => 'Error'], 404);
    }

    public function asignarEquipo(AsignarEquipoRequest $request,$id){
        if ($request->isJson()) {
            $validated = $request->validated();
            $jugador = Jugador::findOrFail($id);
            $jugador->id_equipo = $request->id_equipo;
            $jugador->saveOrFail();
            return response()->json(['status' => 'success', 'jugador' => $jugador]);
        }
        return response()->json(['message' => 'Error'], 404);
    }
}
