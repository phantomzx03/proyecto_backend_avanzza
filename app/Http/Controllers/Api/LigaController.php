<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\GuardarEquiposRequest;
use App\Imports\LigaImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Liga;

class LigaController extends Controller
{
    //
    public function obtenerTodos()
    {
        return response()->json(['status' => 'success', 'ligas' => Liga::with('tipo')->get()]);
    }

    public function obtenerEquipos($id)
    {
        $liga = Liga::where('id', $id)->with(['equipos', 'tipo'])->firstOrFail();
        return response()->json(['status' => 'success', 'liga' => $liga]);
    }

    public function guardarEquipos(GuardarEquiposRequest $request)
    {
        $validated = $request->validated();
        try {
            Excel::import(new LigaImport, $request->file('archivo'));
            return response()->json(['status' => 'success', 'message' => 'Se importó el documento correctamente.'],201);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'error', 'message' => 'No se pudo insertar los datos.'],500);
        }
    }
}
