<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class GuardarJugadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //Valida los campos que se nesecita
        return [
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
            'fecha_nacimiento' => 'required|date_format:Y-m-d'
        ];
    }

    public function messages()
    {
        return [
            'nombres.required' => 'El :attribute es obligatorio.',
            'nombres.string' => 'El :attribute solo debe de tener letras.',
            'apellidos.required' => 'El :attribute es obligatorio.',
            'apellidos.string' => 'El :attribute solo debe de tener letras.',
            'fecha_nacimiento.required' => 'La :attribute es obligatorio.',
            'fecha_nacimiento.date_format' => 'La fecha de nacimiento debe tener el formato "Año-Mes-Día".'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'status' => false,
                'messages' => $validator->errors()->all()
            ], 200)
        );
    }
}
