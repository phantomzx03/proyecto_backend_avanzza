<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoLiga extends Model
{
    //
    protected $table = 'tipos_ligas';

    protected $primaryKey = 'id';

    protected  $fillable = ['nombre'];

    public $timestamps = false;
}
