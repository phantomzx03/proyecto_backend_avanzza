<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{
    //
    protected $table = 'jugadores';

    protected $primaryKey = 'id';

    protected  $fillable = ['nombres','apellidos','fecha_nacimiento'];

    public $timestamps = false;
}
