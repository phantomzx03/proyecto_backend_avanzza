# Proyecto Backend Avanzza
## Por José Zevallos Caycho

----------
## Requisitos
    PHP Versión -> 7.2
    MySQL Versión -> 5.7

## Instalación

Clonar el respositorio

    git clone https://gitlab.com/phantomzx03/proyecto_backend_avanzza.git

Cambiar a la carpeta del repositorio

    cd proyecto_backend_avanzza

Instale todas las dependencias usando composer

    composer install

Copie el archivo env de ejemplo y realice los cambios de configuración necesarios en el archivo .env

    cp .env.example .env

Genere una nueva clave de aplicación

    php artisan key:generate

Crear la base de datos llamado "proyecto_avanzza" y copiar el siguiente codigo en el archivo .env para su conexión

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=proyecto_avanzza
    DB_USERNAME=root
    DB_PASSWORD=

Ejecute las migraciones de la base de datos

    php artisan migrate

Ejecute los datos de prueba para la base de datos

    php artisan db:seed

Inicie el servidor de desarrollo local

    php artisan serve

Ahora puede acceder al servidor en http://localhost:8000

Se puede acceder a la api en [http://localhost:8000/api](http://localhost:8000/api).

## Especificación de API

Para poder hacer uso de las API se nesecita token de autenticación, el tipo de autenticación usado es Bearer Token, estos tokens se encuentran registrados en la base de datos en tabla de api_codigos donde tambien se almacena el tipo si es libre o de paga (identificados como 'L' Libre y 'P' Paga).

## Carpetas

- `app` - Contiene todos los modelos Eloquent
- `app/Http/Controllers/Api` - Contiene todos los controladores api
- `app/Http/Middleware` - Contiene el middleware de autenticación para los tokens
- `app/Http/Requests/Api` - Contiene todas las solicitudes de formulario de API
- `database/migrations` - Contiene todas las migraciones de la base de datos
- `database/seeds` - Contiene los datos de prueba de la base de datos
- `routes` - Contiene todas las rutas api definidas en el archivo api.php
- `excel` - Contiene Excel de prueba para subir masivamente ligas con equipos
- `database_model` - Contiene el modelo de datos
- `tests` - Contains all the application tests
- `tests/Feature/Api` - Contains all the api tests

## Variables de Entorno

- `.env` - Las variables de entorno se pueden configurar en este archivo

----------
# EndPoints

- `http://localhost:8000/api/ligas` - (HTTP = GET) Listado de todas las ligas de fútbol nacional.
- `http://localhost:8000/api/ligas/{id}/equipos` - (HTTP = GET, Parametro id = id de liga) Listado de equipos que participarán de una liga en particular.
- `http://localhost:8000/api/equipos/{id}/ligas` - (HTTP = GET, Parametro id = id de equipo) Listado de todas las ligas en que participará un equipo en particular.
- `http://localhost:8000/api/equipos/{id}/jugadores` - (HTTP = GET, Parametro id = id de equipo) Listado de todos los jugadores de un equipo.
- `http://localhost:8000/api/ligas/guardar-equipos` - (HTTP = POST) Carga masiva de equipos a una liga (Archivo).
- `http://localhost:8000/api/jugadores/{id}/asignar-equipo` - (HTTP = PUT, Parametro id = id de jugador) Incorporación de jugador (ya existente) a un equipo. Para actualizar se debe enviar el dato "id_equipo" en formato JSON.(Ejemplo { "id_equipo" : 4 }).
- `http://localhost:8000/api/jugadores/guardar` - (HTTP = POST) Creación de un jugador. Para registrar se nesecita : nombres(STRING), apellidos(STRING) y fecha_nacimiento(ejemplo : "2021-08-27")(STRING) en formato JSON.(Ejemplo {"nombres" : "Jugador","apellidos" : "16","fecha_nacimiento" : "2021-08-26" }).

**Nota : Todos los EndPoints trabajan con el formato JSON excepto el de carga masiva ya que es nesesario un archivo excel.
----------

# Autenticación
 
El token se pasa con cada solicitud utilizando el encabezado "Autorización" con el esquema "Token". El middleware de autenticación maneja la cantidad de peticiones que se hacen a la API haciendo la validación si el token es de paga(peticiones sin restricciones) o es libre(maximo 3 peticiones).
Laravel incluye un middleware para limitar el acceso a las rutas dentro de nuestra aplicación. Para comenzar, debemos asignar el middleware throttle a una ruta o un grupo de rutas. El middleware throttle acepta dos parámetros que determinan la cantidad máxima de solicitudes que se pueden realizar en un número determinado de minutos.
El archivo esta en la ruta app/Http/Middleware/ApiThrottleRequests.php se copio del archivo Illuminate\Routing\Middleware\ThrottleRequests.php exactamente para hacerla la modificacion en la validación de  peticiones y sus contadores.
 

----------

# Testing API

Inicie el servidor de desarrollo local

    php artisan serve

Ahora se puede acceder a la API en

    http://localhost:8000/api

Ejecutar dentro del proyecto para validar los test

    vendor/bin/phpunit

Request headers

| **Requerido** 	| **Clave**              	| **Valor**            	|
|----------	|------------------	|------------------	|
| Si      	| Content-Type     	| application/json 	|
| Si      	| X-Requested-With 	| XMLHttpRequest   	|
| Si 	| Authorization    	| Bearer Token     	|


----------
