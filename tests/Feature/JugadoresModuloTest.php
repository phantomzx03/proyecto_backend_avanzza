<?php

namespace Tests\Feature;

use App\ApiCodigo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class JugadoresModuloTest extends TestCase
{
    /** @test */
    public function crear_un_jugador()
    {
        $codigo = ApiCodigo::where('tipo','P')->firstOrFail();
        $data = [
            "nombres" => "Jugador",
            "apellidos" => "17",
            "fecha_nacimiento" => "1995-09-18"
        ];
        $this->postJson('/api/jugadores/guardar',$data,['Authorization' => 'Bearer ' . $codigo->token])->assertStatus(201);
    }

    /** @test */
    public function asignar_equipo_a_jugador()
    {
        $codigo = ApiCodigo::where('tipo','P')->firstOrFail();
        $data = [
            //Id de equipo = 1
            "id_equipo" => "1",
        ];
        //Id de jugador = 13
        $this->putJson('/api/jugadores/13/asignar-equipo',$data,['Authorization' => 'Bearer ' . $codigo->token])->assertStatus(200);
    }
}
