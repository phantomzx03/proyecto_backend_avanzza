<?php

namespace Tests\Feature;

use App\ApiCodigo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EquiposModuloTest extends TestCase
{
    /** @test */
    public function mostrar_equipos()
    {
        $codigo = ApiCodigo::where('tipo','P')->firstOrFail();
        $this->get('/api/equipos',['Authorization' => 'Bearer ' . $codigo->token])->assertStatus(200);
    }

    /** @test */
    public function mostrar_ligas_de_equipo()
    {
        $codigo = ApiCodigo::where('tipo','P')->firstOrFail();
        //Equipo con el id = 1
        $this->get('/api/equipos/1/ligas',['Authorization' => 'Bearer ' . $codigo->token])->assertStatus(200);
    }

    /** @test */
    public function mostrar_jugadores_de_equipo()
    {
        $codigo = ApiCodigo::where('tipo','P')->firstOrFail();
        //Equipo con el id = 4
        $this->get('/api/equipos/4/jugadores',['Authorization' => 'Bearer ' . $codigo->token])->assertStatus(200);
    }
}
