<?php

namespace Tests\Feature;

use App\ApiCodigo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class LigasModuloTest extends TestCase
{
    /** @test */
    public function mostrar_ligas()
    {
        $codigo = ApiCodigo::where('tipo', 'P')->firstOrFail();
        $this->get('/api/ligas', ['Authorization' => 'Bearer ' . $codigo->token])->assertStatus(200);
    }

    /** @test */
    public function mostrar_equipos_de_liga()
    {
        $codigo = ApiCodigo::where('tipo', 'P')->firstOrFail();
        //Liga con el id = 5
        $this->get('/api/ligas/5/equipos', ['Authorization' => 'Bearer ' . $codigo->token])->assertStatus(200);
    }

    /** @test */
    public function envio_masivo_ligas_con_equipos()
    {
        $codigo = ApiCodigo::where('tipo', 'P')->firstOrFail();
        $archivo = new UploadedFile(
            base_path('excel/Equipos_A_Liga.xlsx'),
            'Equipos_A_Liga.xlsx',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            null,
            true
        );
        $response = $this->json('POST', '/api/ligas/guardar-equipos', [
            'archivo' => $archivo
        ], ['Authorization' => 'Bearer ' . $codigo->token])->assertStatus(201);
    }
}
